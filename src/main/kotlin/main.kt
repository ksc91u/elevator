import com.google.common.graph.GraphBuilder
import com.google.common.graph.Graphs
import java.util.*

fun main(args : Array<String>) {
//    var graph = GraphBuilder.undirected().build<Int>()
//    graph.addNode(1)
//    graph.addNode(2)
//    graph.addNode(3)
//    graph.addNode(4)
//    graph.putEdge(1, 2 )
//    graph.putEdge(3, 1 )
//    graph.putEdge(2, 4 )
//
//    var graph2 = Graphs.copyOf(graph)
//
//    println(">>> ${graph.nodes()}")
//    println(">>> ${graph2.nodes()}")
//    println(">>> graph === graph2 " + (graph === graph2))

    val e = Elevator()
    e.currentFloor = 5
    e.callAt(10, 1)
    
    assert(e.getNextFloor() == 10)

    e.currentFloor = 5
    e.callAt(3, 0)
    e.callAt(15, 0)
    assert(e.getNextFloor() == 2)

//    var stop = false
//
//    val moveThread = object: Thread() {
//        override fun run() {
//            println(">>> initial status " )
//            e.status()
//            var moves = 0
//            while(true) {
//                moves = moves + 1
//                var action = Random().nextInt(4)
//                val floor = Random().nextInt(20)
//                if (moves >= 10){
//                    action = 3
//                }
//                when (action) {
//                    0 -> e.goTo(floor)
//                    1 -> e.callAt(floor, 0)
//                    2 -> e.callAt(floor, 1)
//                    3 -> {
//                        val moved = e.move()
//                        if(!moved) {
//                            stop = true
//                        }
//                    }
//                }
//                e.status()
//                if(stop) {
//                    break
//                }
//                Thread.sleep(3000)
//            }
//        }
//    }
//
//    moveThread.start()
    //statusThread.start()
   // moveThread.join()
    //statusThread.join()


//
//    e.callAt(10, 1)
//    e.status()
//    e.move()
//    e.status()
//    e.move()
//    e.move()
//    e.status()
//    e.callAt(2, 1)
//    e.callAt(2, 0)
//    e.status()
}