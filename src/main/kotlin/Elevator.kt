import java.util.*
import kotlin.collections.ArrayList

class Elevator{

    val upQueue: ArrayList<Int> = ArrayList()
    val downQueue: ArrayList<Int> = ArrayList()

    var currentFloor: Int = 0
    var currentDirection: Int = 0 //0 down, 1 up

    @Synchronized
    fun callAt(floor: Int, direction: Int){
        val downUp = arrayOf("down", "up")
        println(">>> floor $floor, call ${downUp[direction]}")
        if(direction == 0 && !downQueue.contains(floor)){
            downQueue.add(floor)
            println(">>>> after up: $upQueue, down: $downQueue")
            return
        }
        if(direction == 1 && !upQueue.contains(floor)){
            upQueue.add(floor)
            println(">>>> after up: $upQueue, down: $downQueue")
            return
        }
    }

    @Synchronized
    fun goTo(floor: Int){
        println(">>> go to $floor")
        if(currentFloor == floor){
            return
        }


        val goDirection = if (currentFloor > floor)  0 else 1
        if(goDirection == 0 && upQueue.isEmpty()) {
            currentDirection = 0
        }
        if(goDirection == 1 && downQueue.isEmpty()) {
            currentDirection = 1
        }

        if(goDirection == 0 && !downQueue.contains(floor)){
            downQueue.add(floor)
        }
        if(goDirection == 1 && !upQueue.contains(floor)){
            upQueue.add(floor)
        }
    }

    @Synchronized
    fun status(){
        val downUp = arrayOf("down", "up")
        print(">>> at $currentFloor go ${downUp.get(currentDirection)}, upQ: $upQueue, downQ: $downQueue," )
        val next = getNextFloor()
        println(" next $next")
        if(currentDirection == 0){
            downQueue.add(next)
        }else{
            upQueue.add(next)
        }
    }

    @Synchronized
    fun move(): Boolean {
        if(upQueue.isEmpty() && downQueue.isEmpty()){
            return false
        }
        val next = getNextFloor()
        val nextDirection = if(next > currentFloor) 1 else 0
        currentDirection = nextDirection

        if(currentDirection == 0) { //down
            val next = getNextFloor()
            currentFloor = currentFloor - 1
            if(currentDirection == next){
                println(">>> open $currentFloor")
            }else{
                downQueue.add(next)
            }
        }else{
            val next = getNextFloor()
            currentFloor = currentFloor + 1
            if(currentDirection == next){
                println(">>> open $currentFloor")
            }else{
                upQueue.add(next)
            }
        }
        return true
    }

    @Synchronized
    fun getNextFloor() : Int{

        if(downQueue.isEmpty() && upQueue.isEmpty()){
            return currentFloor
        }

        var nextFloor = currentFloor
        if(currentDirection == 0){
            if(downQueue.isEmpty()){
                currentDirection = 1
                return getNextFloor()
            }
            nextFloor = downQueue.min()!!

            downQueue.forEach {
                if(it < currentFloor && it >= nextFloor){
                    nextFloor = it
                }
            }

            downQueue.remove(nextFloor)
        }

        if(currentDirection == 1) {
            if(upQueue.isEmpty()){
                currentDirection = 0
                return getNextFloor()
            }
            nextFloor = upQueue.max()!!

            upQueue.forEach {
                if(it > currentFloor && it <= nextFloor){
                    nextFloor = it
                }
            }

            upQueue.remove(nextFloor)
        }

        return nextFloor
    }


}