import org.junit.Test

class SomeUITests {

    //cases
    // go [down, up]
    // out side press [below, up] current floor, direction [up, down]
    // 2*2*2 cases
    // test1 ~ 4

    // go [down, up]
    // inside press [below, up] current floor
    // 2 * 2 cases
    // test 5 ~ 7

    @Test
    fun test1() {
        val e = Elevator()
        e.currentFloor = 5
        e.goTo(1)
        assert(e.currentDirection == 0)
        e.callAt(10, 1)
        assert(e.getNextFloor() == 1)

        assert(e.getNextFloor() == 10)
    }

    @Test
    fun test2() {
        val e = Elevator()
        e.currentFloor = 5
        e.goTo(1)
        assert(e.currentDirection == 0)
        e.callAt(4, 1)
        assert(e.getNextFloor() == 1)
    }

    @Test
    fun test3() {
        val e = Elevator()
        e.currentFloor = 5
        e.goTo(10)
        assert(e.currentDirection == 1)
        e.callAt(3, 1)
        assert(e.getNextFloor() == 10)
    }

    @Test
    fun test3_1() {
        val e = Elevator()
        e.currentFloor = 5
        e.currentDirection = 0
        e.goTo(10)
        assert(e.currentDirection == 1)
        e.callAt(7, 1)
        assert(e.getNextFloor() == 7)
    }

    @Test
    fun test3_2() {
        val e = Elevator()
        e.currentFloor = 5
        e.currentDirection = 0
        e.goTo(10)
        assert(e.currentDirection == 1)
        e.callAt(7, 0)
        assert(e.getNextFloor() == 10)
    }

    @Test
    fun test4() {
        val e = Elevator()
        e.currentFloor = 5
        e.goTo(1)
        assert(e.currentDirection == 0)
        e.callAt(3, 0)
        assert(e.getNextFloor() == 3)
    }

    @Test
    fun test4_1() {
        val e = Elevator()
        e.currentFloor = 5
        e.goTo(1)
        e.callAt(3, 1)
        assert(e.getNextFloor() == 1)
    }

    @Test
    fun test5() {
        val e = Elevator()
        e.currentFloor = 5
        e.goTo(1)
        assert(e.getNextFloor() == 1)
        e.goTo(3)
        assert(e.getNextFloor() == 3)
    }

    @Test
    fun test6() {
        val e = Elevator()
        e.currentFloor = 5
        e.goTo(1)   // if i run getNextFloor here, 1 will be pop out
        e.goTo(7)
        assert(e.getNextFloor() == 1)
    }

    @Test
    fun test7() {
        val e = Elevator()
        e.currentFloor = 5
        e.goTo(10) // if i run getNextFloor here, 10 will be pop out
        e.goTo(3)
        assert(e.getNextFloor() == 10)
    }

}